# Resources
* https://phauer.com/2018/best-practices-unit-testing-kotlin/
* https://spring.io/guides
* https://spring.io/guides/tutorials/spring-boot-kotlin/
* http://reactivex.io/tutorials.html
* https://www.freecodecamp.org/news/rxandroid-and-kotlin-part-1-f0382dc26ed8/
* https://www.packtpub.com/au/application-development/reactive-programming-kotlin
* https://kotlin.link
* https://docs.gradle.org/current/userguide/java_testing.html
* https://medium.com/@magnus.chatt/why-you-should-totally-switch-to-kotlin-c7bbde9e10d5
* https://kotlinlang.org/docs/reference/basic-syntax.html
* https://spring.io/blog/2016/02/15/developing-spring-boot-applications-with-kotlin
* https://spring.io/blog/2017/01/04/introducing-kotlin-support-in-spring-framework-5-0
* https://kotlinlang.org/docs/tutorials/

# What is the learning curve for a Java developer transitioning to Kotlin
1.  Not using JUnit (plus Mockito) but rather kotlintest (plus Mockito-Kotlin) for unit and integration testing
2.  Abandoning Eclipse and learning IntelliJ (for those devs that have only been on projects where they had to use Eclipse)
3.  Abandoning Maven and using Gradle. Gradle works in a fundamentally different way to Maven and uses a different DSL.
4.  Abandoning JPA and using Kotlin Exposed DSL (although it is possible to use JPA, it makes sense to fully embrace all available Kotlin features)
5.  Using/learning Kotlin stdlib
6.  Generally: New annotations, jackson-module-kotlin (JSON parsing), Retrofit/Fuel (HTTP Client), spring-kotlin (DI), handling of nulls and NPEs, operator overloading,
    Object expressions, Companion objects (along with package-level functions and properties), Higher-order functions, star-projection syntax,
    Variance (Generics), new reflection API, construction of DSLs, Coroutines, lateinit, Contracts, kotlinx.serialization, new Coding Conventions, ranges,
    Destructuring Declarations, lambdas with receivers
7.  Functional programming (Java 1.8 used it a bit, but in Kotlin it is a major focus and much more important). Learning idiomatic Kotlin and using some more 
    of its advanced features can take a while to get your head around.
8.  Java interoperability issues (Calling Java code from Kotlin and Calling Kotlin code from Java)
9.  New access modifiers and defaults (public/protected/private, etc)
10. Different way in Defining Classes/Interfaces
11. Mapping Java types to Kotlin types
12. Kotlin claims to fully support all existing Java frameworks, but using these from Kotlin source code is non-trivial (anyway, the preferred approach, for a 
    better effect, will be to rather use the pure Kotlin tools/libraries)
13. RxKotlin and async programming (Flex and Mono).

# When is [Rx](<https://en.wikipedia.org/wiki/Reactive_extensions>) appropriate?
Rx offers a natural paradigm for dealing with sequences of events. A sequence can contain zero or more events. Rx proves to be most valuable when composing 
sequences of events.
  
## Should use Rx
Managing events like these is what Rx was built for:  
* UI events like mouse move, button click
* Domain events like property changed, collection updated, "Order Filled", "Registration accepted" etc.
* Infrastructure events like from file watcher, system and WMI events
* Integration events like a broadcast from a message bus or a push event from WebSockets API or other low latency middleware like Nirvana
* Integration with https://en.wikipedia.org/wiki/Complex_event_processing

## Could use Rx
Rx can also be used for asynchronous calls. These are effectively sequences of one event. Rx can be used, but is less suited for, introducing and managing concurrency 
for the purposes of scaling or performing parallel computations. Other dedicated frameworks like TPL (Task Parallel Library) or C++ AMP are more appropriate for 
performing parallel compute intensive work.

## Won't use Rx
Rx and specifically IObservable<T> is not a replacement for IEnumerable<T>. I would not recommend trying to take something that is naturally pull based and force it to 
be push based. And Message queues.

# IntelliJ keys
- ^Shift-J        join lines 
- Cmd-D           duplicate lines 
- ^Space          Code completion 
- Cmd-Shift-F     Search
- Cmd-bs          Delete line
- Shift-Cmd-F12   Toggle fullscreen
- Cmd-L           goto line
- Shift-Opt-Up/Dn Move line up/down
- Opt-Cmd-L       Reformat code/file
- ^Opt-O          Organise imports
- Shift-Cmd-bs    Goto last edit position
- ^-Up/Down       Goto prev/next method
- ^-J             Show quick help
- Opt-Cmd-`       Next Window (project)
