package learn.kotlin

class Rectangle(val h: Int, val l: Int) {

    val isSquare: Boolean
        get() {
            return h == l
        }
}

enum class Color {
    RED, BLUE
}

fun colorAsText(color: Color) =
    when (color) {
        Color.RED -> "red"
        Color.BLUE -> "blue"
    }
